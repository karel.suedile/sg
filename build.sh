#!/bin/bash

if [ "$#" != "1" ]; then
  echo "Usage: $0 <tag>"
  echo "  Example: $0 1.0.2"
  exit 1;
fi

TAG=$1

echo "docker build --tag "superkeil/sg:front-$TAG" --tag "superkeil/semwee:sg-front-latest" ."
docker build --tag "superkeil/semwee:sg-front-$TAG" --tag "superkeil/semwee:sg-front-latest" .

echo "docker push superkeil/semwee:sg-front-$TAG"
docker push superkeil/semwee:sg-front-$TAG

