import moment from 'moment';

export class SgExport {
    public items;

    public constructor(csv: string) {
        // date_comptabilisation;libellé_complet_operation;montant_operation;devise;
        // 08/04/2022;CARTE X3245 07/04 DIM SUN;-19,00;EUR;

        const lines: string[] = csv.split('\n').filter(line => line.trim() !== '');
        lines.shift();
        lines.shift();
        const items = lines.map((line: string) => {
            const parts: string[] = line.split(/;/g);
            return {
                date: moment(parts[0], 'DD/MM/YYYY').format('YYYY-MM-DD'),
                label: parts[1],
                amount: Number(parts[2].replace(',', '.')),
            };
        });

        const acc = [];
        items.forEach((cur) => {
            let item = acc.find(i => i.date === cur.date);
            if (!item) {
                item = {
                    date: cur.date,
                    operations: []
                };
                acc.push(item);
            }
            item.operations.push({
                label: cur.label,
                amount: cur.amount
            });
            item.amount = Math.round(100 * item.operations.reduce((a, i) => a + i.amount, 0)) / 100;
        });
        this.items = acc;
    }

    public computeCurrentAmount(amount: number) {
        const items = [];
        let currentAmount: number = amount;
        for (const item of this.items) {
            items.push({
                date: item.date,
                operations: item.operations,
                amount: item.amount,
                total: currentAmount
            });
            currentAmount = Math.round(100*(currentAmount - item.amount)) / 100;
        }

        return items.reverse();
    }
}
