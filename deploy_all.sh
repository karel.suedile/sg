#!/bin/bash

PROJECT_NAME=sg
PROJECT_PROD_WEBSITE=www.keil-informatique.com

SHORT_BRANCH=`echo $1 | sed s/[^a-zA-Z0-9]/-/g`
CI_COMMIT_BRANCH=$PROJECT_NAME-$SHORT_BRANCH
CI_COMMIT_SHA=$2
DOCKERHUB_LOGIN=$3
DOCKERHUB_PASSWORD=$4
DOCKERHUB_EMAIL=$5

HOSTURL=$SHORT_BRANCH.keil-informatique.com

TAG=$1-$2
echo "Tag = $TAG"

SED_BACKUP_EXT=""
if [ "$Apple_PubSub_Socket_Render" != "" ]; then
  # si on est sur mac on a besoin de ce parametre supplémentaire
  SED_BACKUP_EXT=".bak"
fi

kubectl get namespace $CI_COMMIT_BRANCH
[ $? -eq 0 ] && echo "Namespace already existing" || kubectl create namespace $CI_COMMIT_BRANCH

kubectl config set-context --current --namespace=$CI_COMMIT_BRANCH

kubectl get secret regcred
[ $? -eq 0 ] && echo "Secret already existing" || kubectl create secret docker-registry regcred --docker-server=docker.io --docker-username=$DOCKERHUB_LOGIN --docker-password=$DOCKERHUB_PASSWORD --docker-email=$DOCKERHUB_EMAIL

echo "front"

sed -i $SED_BACKUP_EXT "s/front-latest/front-$TAG/g" helm/values.yaml
if [ "$CI_COMMIT_BRANCH" != "$PROJECT_NAME-main" ]; then
  sed -i $SED_BACKUP_EXT "s/$PROJECT_PROD_WEBSITE/$HOSTURL/g" helm/values.yaml
fi
cat helm/values.yaml

echo "helm upgrade --install --wait --set image.tag=sg-front-$TAG $PROJECT_NAME-front helm/"
helm upgrade --install --wait --set image.tag=sg-front-$TAG $PROJECT_NAME-front helm/
