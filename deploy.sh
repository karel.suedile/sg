#!/bin/bash

if [ "$#" != "1" ]; then
  echo "Usage: $0 <tag>"
  echo "  Example: $0 1.0.2"
  exit 1;
fi

TAG=$1

echo "helm upgrade --install --wait --set image.tag=sg-front-$TAG sg-front helm/"
helm upgrade --install --wait --set image.tag=sg-front-$TAG sg-front helm/
